# -*- coding: utf-8 -*-
# Copyright 2018 Prosoft
# License LGPL-3.0 or later (http://www.gnu.org/licenses/LGPL).

from odoo import api, fields, models, _, tools
from odoo import http, tools, _
from odoo.http import request


class client(models.Model):
    _inherit = "res.partner"

    @api.model
    def get_client(self, id):
        id_client = self.env['res.partner'].search([('id', '=', int(id) )])
        childrens_dict = {}
        childrens_list = []
        if id_client:
            childrens_dict.update(
                {
                    'client_id': id_client.id, 'name': id_client.name
                })

            childrens_list.append(dict(childrens_dict))


        return childrens_list
