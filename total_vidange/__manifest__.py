{'name': "Total Vidange",
 'version': '1.0',
 "depends": ['base', 'stock', 'sale', 'purchase', 'point_of_sale','calendar', ],
 'author': "ProoSoft Cloud",
 'category': '',
 'sequence': 1,
 'summary': 'Module qui gére la gestion du station de service',
 'description': "L’objectif général de cette étude est de mettre en évidence l'utilisation de huile de moteur des voiture et de la consommation énergétique en concevant en fin de compte un nouveau modèle de station-service Total plus économe et utilisant au mieux des sources d’énergie propre. ",
 # data files always loaded at installation
 'data': [
     'security/account_security.xml',
     'security/ir.model.access.csv',
     'views/menu_item.xml',
     'views/client.xml',
     'views/admin.xml',
     'views/secretaire.xml',
     'views/inhirite/pos_config_view_form.xml',
     'views/calendar.xml',
     'views/add_secretaire.xml',
     'views/voiture.xml',
     'views/client_acces.xml',
     'views/inhirite/calendre.xml',

 ],
 # data files containing optionally loaded demonstration data
 'demo': [
     '',
 ],
 'installable': True,
 'application': True,
 'auto_install': False,

 }
