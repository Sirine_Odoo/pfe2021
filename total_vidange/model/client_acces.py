from odoo import fields, models, exceptions,_
from odoo import api


class client_acces(models.Model):
    _name = 'res.partner.line'

    partner_id=fields.Many2one('res.partner', string="Partner", readonly=False)
    user_id = fields.Many2one('res.users', string='User' , store=True, readonly=False)
    name = fields.Char(string="Name" ,related='partner_id.name', store=True)
    email = fields.Char(string="Email",related='partner_id.email', store=True)
    phone = fields.Char(string="Phone",related='partner_id.phone', store=True)
    client_voiture  = fields.Many2one(string="Voiture client",related='partner_id.client_voiture', store=True)


    #
    # modele_veh  = fields.Char(string="Modéle voiture",related='partner_id.modele_veh', store=True)
    # matricule_veh  = fields.Char(string="Matricule voiture",related='partner_id.matricule_veh', store=True)

    # @api.onchange('partner_id', 'user_id')
    # def add_nem_email(self):
    #     self.name=self.partner_id.name
    #     self.email=self.partner_id.email