from odoo import fields, models
from odoo import api

class ResUsersInheret(models.Model):
    _inherit = 'res.users'


    is_client_total = fields.Boolean('C est un client Totale')
    is_secretaire = fields.Boolean('C est un secretaire')