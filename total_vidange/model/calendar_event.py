# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models


class CalendarEventInheret(models.Model):

    _inherit = "calendar.event"

    state = fields.Selection([('draft', 'Unconfirmed'), ('open', 'Confirmed'), ('progress', 'En cours')], string='Status',  tracking=True, default='progress')

    def action_confirmed (self):
        self.state="open"
        product_ctx = dict(default_res_id=self.id)
        activity_id = self.env['mail.activity'].create({
            'summary': 'Meeting with Client',
            'activity_type_id': '2',
            'res_model_id': self.env['ir.model'].search([('model', '=', 'calendar.event')], limit=1).id,
            'res_id': self.id,
            'user_id': 13,

        }).with_context(product_ctx)

        print('test')


    def action_cancel (self):
        self.state = "draft"