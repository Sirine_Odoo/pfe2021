from odoo import fields, models, exceptions,_
from odoo import api


class client_total(models.Model):
    _inherit = 'res.partner'
    _description = 'Client Record'

    is_client_total = fields.Boolean('C est un client Totale')

    matricule_veh = fields.Char(string="Matricule du voiture")
    modele_veh = fields.Char(string="Modéle du voiture")
    ddv_veh = fields.Date(string="Date du derniére viste du voiture", default=fields.Date.context_today)
    client_fiche_line = fields.One2many('line.fiche.client', 'client', string='LISTE DES Fichiers client')
    client_voiture = fields.Many2one('voiture.client', string='Voiture Client')
    is_secretaire = fields.Boolean('C est un secretaire')

    def add_rdv(self):
        return {
            'name': 'self.order_id',
            'res_model': 'calendar.event',
            'type': 'ir.actions.act_window',
            'context': {},
            'view_mode': 'form',
            'view_type': 'form',
            'view_id': self.env.ref("total_vidange.form_view_calendar_client").id,
            'target': 'new'
        }

    # @api.model
    def create_access(self):
        for record in self:
            if record.email:

                user = self.env['res.users'].create({
                    'partner_id': record.id,
                    'login': record.email,
                    'password': '123',
                    'groups_id': [(6, 0, [self.env.ref('total_vidange.group_client_security').id])]

                })
                add_client = self.env['res.partner.line'].sudo().create({
                    'partner_id': record.id,
                    'user_id' : user.id,

                })

                # user.groups_id = [(6, 0, self.env.ref('total_vidange.action_client_new').id)]
            else:
                raise exceptions.ValidationError(_('"saisire votre email merciiiiiiii.'))
        return True


class client_fiche(models.Model):
    _name = 'line.fiche.client'
    active = fields.Boolean(default=True)
    client = fields.Many2one('res.partner')
    date = fields.Date(string="Date", default=fields.Date.context_today)
    huile = fields.Boolean(default=False)
    article = fields.Many2many('product.template', 'product_template_client_rel', 'article_id', 'client_id',
                               string="article")


class voiture_client(models.Model):
    _name = 'voiture.client'
    active = fields.Boolean(default=True)
    name = fields.Char("Matricule du voiture")
    marque = fields.Char('marque voiture')
    type_denergie = fields.Selection(
        [('Diesel', 'Diesel'), ('Essence', 'Essence'), ('Hybride', 'Hybride'), ('Electrique', 'Electrique')],
        string='type dénergie ', tracking=True)
    kilometrage = fields.Float('Kilometrage de vehicule')
    puissance = fields.Char('Puissance')
